<?php
/**
 * @file
 * Provides the context CTools plugin base class.
 */

abstract class ContextConditionResolution extends context_condition {

  /**
   * The name of the cookie used.
   * @var string
   */
  protected $cookieName;

  /**
   * Condition values.
   */
  public function condition_values() {
    return array(
      1 => 'Enabled',
    );
  }

  /**
   * Provide from/until dimension fields.
   */
  public function options_form($context = NULL) {

    $defaults = $this->fetch_from_context($context, 'options');

    $form['width_from'] = array(
      '#type' => 'textfield',
      '#title' => t('Width from'),
      '#description' => t('In pixels. Leave empty for no boundry.'),
      '#default_value' => isset($defaults['width_from']) ? $defaults['width_from'] : '',
    );
    $form['width_until'] = array(
      '#type' => 'textfield',
      '#title' => t('Width until'),
      '#description' => t('In pixels. Leave empty for no boundry.'),
      '#default_value' => isset($defaults['width_until']) ? $defaults['width_until'] : '',
    );

    $form['height_from'] = array(
      '#type' => 'textfield',
      '#title' => t('Height from'),
      '#description' => t('In pixels. Leave empty for no boundry.'),
      '#default_value' => isset($defaults['height_from']) ? $defaults['height_from'] : '',
    );
    $form['height_until'] = array(
      '#type' => 'textfield',
      '#title' => t('Height until'),
      '#description' => t('In pixels. Leave empty for no boundry.'),
      '#default_value' => isset($defaults['height_until']) ? $defaults['height_until'] : '',
    );

    return $form;
  }

  /**
   * Get the options for a context, with filled in default values.
   *
   * @return array
   *   Assoc array with options.
   */
  protected function getOptions($context = NULL) {
    $keys = array(
      'width_from',
      'width_until',
      'height_from',
      'height_until',
    );

    $options = $this->fetch_from_context($context, 'options');

    // Ensure that all keys are present and numeric values are filled in.
    foreach ($keys as $key) {
      if (!isset($options[$key]) || !is_numeric($options[$key])) {
        $from_key = ($key === 'width_from' || $key === 'height_from');

        $options[$key] = $from_key ? 0 : 9999999999;
      }
    }

    return $options;
  }

  /**
   * Execute.
   */
  public function execute() {
    $resolution = NULL;

    // Check if there is a valid resolution stored in the cookie.
    if (isset($_COOKIE[$this->cookieName])) {
      $resolution = explode(',', $_COOKIE[$this->cookieName]);

      if (count($resolution) === 2 && is_numeric($resolution[0]) && is_numeric($resolution[1])) {
        // A valid resolution was found.
        $resolution = array(
          'width' => (int) $resolution[0],
          'height' => (int) $resolution[1],
        );
      }
    }
    if ($resolution) {
      foreach ($this->get_contexts() as $context) {
        $options = $this->getOptions($context);

        $width_match = $resolution['width'] >= $options['width_from'] && $resolution['width'] <= $options['width_until'];
        $height_match = $resolution['height'] >= $options['height_from'] && $resolution['height'] <= $options['height_until'];

        if ($width_match && $height_match) {
          $this->condition_met($context);
        }
      }
    }
  }
}
