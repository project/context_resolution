<?php
/**
 * @file
 * Provides the context CTools plugin class for the browser resolution
 * condition.
 */

class ContextConditionBrowserResolution extends ContextConditionResolution {
  protected $cookieName = 'context_browserresolution';
}
