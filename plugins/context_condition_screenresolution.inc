<?php
/**
 * @file
 * Provides the context CTools plugin class for the browser resolution
 * condition.
 */

class ContextConditionScreenResolution extends ContextConditionResolution {
  protected $cookieName = 'context_screenresolution';
}
