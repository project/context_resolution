
(function ($) {
  Drupal.behaviors.contextResolution = {
    attach: function (context, settings) {
      var that = this;

      // Update cookies on each resize.
      $(window).resize(function() {
        that.updateCookies();
      });

      // Do a first manual cookie update to catch the current width.
      this.updateCookies();
    },

    updateCookies: function() {
      var date = new Date();
      date.setTime(date.getTime() + 30*24*60*60*1000);

      var expires = date.toGMTString();

      // Set cookie for screen resolution.
      document.cookie = 'context_screenresolution=' + screen.width + ',' + screen.height + '; expires=' + expires + '; path=/';

      // Set cookie for browser window resolution.
      $window = $(window);
      document.cookie = 'context_browserresolution=' + $window.width() + ',' + $window.height() + '; expires=' + expires + '; path=/';
    }
  };
})(jQuery);
